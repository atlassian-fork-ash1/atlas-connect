#!/usr/bin/env node

var path = require('path');
var fs = require('fs');
var inquirer = require('inquirer');
var extend = require('extend');
var program = require('commander');
var S = require('string');
var generator = require(path.join(__dirname, '..', 'lib', 'generator'));

var templatePattern = /^(jira|jira-service-desk|confluence|hipchat|bitbucket|https?:\/\/.*)$/;
program
    .option('-t, --template <template>', 'the template to use (jira|jira-service-desk|confluence|hipchat|bitbucket) or the url to download a custom template .zip from')
    .parse(process.argv);

var args = program.args;
var name = null;
if (args && args.length > 0) {
    name = args[0];
}

var template = program.template;

if (typeof template !== 'string') {
    template = null;
} else if (template) {
    template = template.toLowerCase();
    if (!templatePattern.test(template)) {
        generator.logError("The template requested isn't valid. Please select from one of (jira|jira-service-desk|confluence|hipchat|bitbucket)" +
        " or provide an https:// url to a custom template .zip file.");
        return;
    }
}

var opts = {
    name: name,
    template: template
};
var questions = [];

if (!name) {
    questions.push({
        type: 'input',
        name: 'name',
        message: 'What would you like to name your project?',
        validate: function (input) {
            if (S(input).isEmpty()) {
                return 'Please enter a non empty value';
            }
            return true;
        },
        filter: function (value) {
            return S(value).trim().s;
        }
    });
}

if (!template) {
    questions.push({
        type: 'list',
        name: 'template',
        message: 'Select a product',
        choices: ['jira', 'jira-service-desk', 'confluence', 'hipchat', 'bitbucket']
    });
}

var promise;
if (questions.length) {
    promise = inquirer.prompt(questions);
} else {
    promise = Promise.resolve([]);
}

promise.then(function (answers) {
    opts = extend(opts, answers);
    generator.createTemplate(opts.name, opts.template);
});